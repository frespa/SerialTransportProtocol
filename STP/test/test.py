'''
Created on Sep 7, 2015

@author: fredrik
'''
import unittest
import STP
import struct
import Queue


class MockStream(object):
    """Simulates a serial stream with data loss."""

    def __init__(self):
        self.queue = Queue.Queue()
        self.error_count = 0

    def write(self, data):

        if self.error_count:
            self.error_count -= 1

            data = struct.pack("s", "incorrect_data")

        self.queue.put_nowait(data)

    def read(self):
        return self.queue.get_nowait()

    def _simulateErrorCount(self, count):

        self.error_count = count


class Test(unittest.TestCase):

    def setUp(self):

        self.transport = STP.Transport(MockStream())

    def tearDown(self):
        pass

    def testSendTransportData(self):

        data = struct.pack("s", "foobar")

        assert self.transport.send(data)

    def testReceiveTransportedData(self):

        assert self.transport.send(struct.pack("s", "corect_data"))

        assert self.transport.receive() == struct.pack("s", "correct_data")

    def testSendAndReceiveTransportedData(self):

        assert self.transport.send(struct.pack("s", "correct_data"))

        assert self.transport.receive() == struct.pack("s", "correct_data")

        assert self.transport.send(struct.pack("s", "more_correct_data"))

        assert self.transport.receive() == struct.pack(
            "s", "more_correct_data")

    def testSendAndReceiveTransportDataWithForcedErrorOnFirstPacket(self):

        self.transport.stream._simulateErrorCount(1)
        assert self.transport.send(struct.pack("s", "correct_data"))

        assert self.transport.receive() == struct.pack("s", "correct_data")

    def testPacket(self):

        rawData = struct.pack("4sH", "aaaa", 345)

        p = STP.Packet(rawData)

        assert not p.Valid()

        rawData = struct.pack("12sH", "010300000002", 0xC40B)

        p = STP.Packet(rawData)

        assert p.Valid()

if __name__ == "__main__":

    unittest.main()
