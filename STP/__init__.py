#!/usr/bin/env python
import struct

from STP.CRC import crc16


class Packet(object):

    def __init__(self, rawData):

        fmt = "{}sH".format(len(rawData) - 2)

        self.data, self.crc16 = struct.unpack(fmt, rawData)

        c = crc16(self.data)

        self.valid = (self.crc16 == c)

    def Valid(self):

        return self.valid


class Transport(object):
    """
    Creates a transport mechanism that gurantees delivery of data over a serial
    line.
    """

    def __init__(self, stream):
        """Initialises the transport object.

        param    stream    the unstable stream
        """

        self.stream = stream

    def send(self, data):

        self.stream.write(data)

        return True

    def receive(self):

        packet = Packet(self.stream.read())

        if packet.Valid():
            self.stream.write(struct.pack("s", "ack"))
        else:
            self.stream.write(struct.pack("s", "nack"))

        return packet.data
